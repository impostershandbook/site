## The Public Site for ImpostersHandbook.com

If you'd like to help with typos, etc, please do! It's just a jekyll site, so you can change things easily. The index.html page is the only page on the site. The chapter info, stats and so on are in the /data folder as YML files.

Thanks!

Rob
